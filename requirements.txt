pbr == 5.8.0  # Needed for pbr.version
setuptools == 60.2.0  # Needed to workaround https://github.com/pyinstaller/pyinstaller/issues/4984
bunq-sdk == 1.14.18
click == 8.0.3
validators == 0.18.2
tenacity == 8.0.1
python-dateutil == 2.8.2
